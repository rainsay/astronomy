package me.rainsay.astronomy;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import me.rainsay.astronomy.model.Coordinate;
import me.rainsay.astronomy.utils.JDUtils;
import me.rainsay.astronomy.utils.NutationUtils;
import me.rainsay.astronomy.utils.VSOP87;

/**
 * Unit test for simple App.
 */
public class AppTest {
    
    @ParameterizedTest(name = "Time {index} :")
    @ValueSource(strings = {"2000-1-1 12:00:00", "1899-12-31 12:00:00", "1799-12-30 12:00:00", "1699-12-29 12:00:00", "1599-12-29 12:00:00", "1499-12-19 12:00:00"})
    public void JDTest(String dstr) {
        String timeZone = "GMT+0";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone(timeZone));
        //String dstr = "2000-1-1 12:00:00";
        Date date;
        try {
            date = df.parse(dstr);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("时间转换失败");
        }
        double i = JDUtils.julianCal(date);
        Coordinate c = VSOP87.calCoordinate(i);
        System.out.println("Date: "+ dstr + ", JD:" +i + ", " + c);
    }

    @Test
    public void jdFromTest() {
        String timeZone = "GMT+8";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone(timeZone));
        String dstr = "2001-3-1 12:00:00";
        Date date;
        try {
            date = df.parse(dstr);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("时间转换失败");
        }
        double jd = JDUtils.julianCal(date);
        System.out.println(JDUtils.utcFromJD(jd));
        System.out.println(JDUtils.fromJujian(jd));
    }

    @ParameterizedTest
    @ValueSource(strings = {
     "2019-2-4 12:00:00",
     "2019-2-19 12:00:00", 
     "2019-3-6 12:00:00", 
     "2019-3-21 12:00:00"})
    public void nuationTest(String dstr) {
        String timeZone = "GMT+0";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone(timeZone));
        // String dstr = "2000-1-1 12:00:00";
        Date date;
        try {
            date = df.parse(dstr);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("时间转换失败");
        }
        double jd = JDUtils.julianCal(date);
        Coordinate coor = VSOP87.calCoordinate(jd);
        double angle = ((coor.getL() + Math.PI)/NutationUtils.RAD) + 180;
        angle = angle % 360;
        System.out.println(angle);
    }

    @Test
    public void solarTest() {
        int year = 2019;
        // double jd = NutationUtils.calcSolarTerms(year, 15);
        // System.out.println("力学时:"  + JDUtils.fromJujian(jd) + ";UTC: " + JDUtils.utcFromJD(jd));
        
        for(int i = 0; i < 24; i++) {
            int angle = i * 15;
            double jd = NutationUtils.calcSolarTerms(year, angle);
            System.out.println("力学时:"  + JDUtils.fromJujian(jd) + ";UTC: " + JDUtils.utcFromJD(jd));
        }
    }

    @Test
    public void deltatTest() {
        double jd = 2458198.5114811133;
        jd -= JDUtils.deltatT(jd);
        System.out.println(jd);
    }

    @Test
    public void nuaTest() {
        int year = 2019;
        double jd = 365.2422 * (year - 2000) + JDUtils.JD2000;
        Date d = JDUtils.fromJujian(jd);
        System.out.println(d);
    }
}

package me.rainsay.astronomy.utils;

import java.util.Calendar;
import java.util.TimeZone;

import me.rainsay.astronomy.model.Coordinate;

/**
 * 地球章动计算
 */
public class NutationUtils {

    public static double[][] nuationTable = new double[][]{
        {0, 0, 0, 0, 1,     -171996,    -174.2, 92025,  8.9},
        {-2, 0, 0, 2, 2,    -13187,     -1.6,   5736,   -3.1},
        {0, 0, 0, 2, 2,     -2274,      -0.2,   977,    -0.5},
        {0, 0, 0, 0, 2,     2062,       0.2,    -895,   0.5},
        {0, 1, 0, 0, 0,     1426,       -3.4,   54,     -0.1},
        {0, 0, 1, 0, 0,     712,        0.1,    -7,     0},
        {-2, 1, 0, 2, 2,    -517,       1.2,    224,    -0.6},
        {0, 0, 0, 2, 1,     -386,       -0.4,   200,    0},
        {0, 0, 1, 2, 2,     -301,       0,      129,    -0.1},
        {-2, -1, 0, 2, 2,   217,        -0.5,   -95,    0.3},
        {-2, 0, 1, 0, 0,    -158,       0,      0,      0},
        {-2, 0, 0, 2, 1,    129,        0.1,     -70,   0},
        {0, 0, -1, 2, 2,    123,        0,      -53,    0},
        {2, 0, 0, 0, 0,     63,         0,      0,      0},
        {0, 0, 1, 0, 1,     63,         0.1,    -33,    0},
        {2, 0, -1, 2, 2,    -59,        0,      26,     0},
        {0, 0, -1, 0, 1,    -58,        -0.1,   32,     0},
        {0, 0, 1, 2, 1,     -51,        0,      27,     0},
        {-2, 0, 2, 0, 0,    48,         0,      0,      0},
        {0, 0, -2, 2, 1,    46,         0,      24,     0},
        {2, 0, 0, 2, 2,     -38,        0,      16,     0},
        {0, 0, 2, 2, 2,     -31,        0,      13,     0},
        {0, 0, 2, 0, 0,     29,         0,      0,      0},
        {-2, 0, 1, 2, 2,    29,         0,      -12,    0},
        {0, 0, 0, 2, 0,     26,         0,      0,      0},
        {-2, 0, 0, 2, 0,    -22,        0,      0,      0},
        {0, 0, -1, 2, 1,    21,         0,      -10,    0},
        {0, 2, 0, 0, 0,     17,         -0.1,   0,      0},
        {2, 0, -1, 0, 1,    16,         0,      -8,     0},
        {-2, 2, 0, 2, 2,    -16,        0.1,    7,      0},
        {0, 1, 0, 0, 1,     -15,        0,      9,      0},
        {-2, 0, 1, 0, 1,    -13,        0,      7,      0},
        {0, -1, 0, 0, 1,    -12,        0,      6,      0},
        {0, 0, 2, -2, 0,    11,         0,      0,      0},
        {2, 0, -1, 2, 1,    -10,        0,      5,      0},
        {2, 0, 1, 2, 2,     -8,         0,      3,      0},
        {0, 1, 0, 2, 2,     7,          0,      -3,     0},
        {-2, 1, 1, 0, 0,    -7,         0,      0,      0},
        {0, -1, 0, 2, 2,    -7,         0,      3,      0},
        {2, 0, 0, 2, 1,     -7,         0,      3,      0},
        {2, 0, 1, 0, 0,     6,          0,      0,      0},
        {-2, 0, 2, 2, 2,    6,          0,      -3,     0},
        {-2, 0, 1, 2, 1,    6,          0,      -3,     0},
        {2, 0, -2, 0, 1,    -6,         0,      3,      0},
        {2, 0, 0, 0, 1,     -6,         0,      3,      0},
        {0, -1, 1, 0, 0,    5,          0,      0,      0},
        {-2, -1, 0, 2, 1,   -5,         0,      3,      0},
        {-2, 0, 0, 0, 1,    -5,         0,      3,      0},
        {0, 0, 2, 2, 1,     -5,         0,      3,      0}, 
        {-2, 0, 2, 0, 1,    4,          0,      0,      0},
        {-2, 1, 0, 2, 1,    4,          0,      0,      0},
        {0, 0, 1, -2, 0,    4,          0,      0,      0},
        {-1, 0, 1, 0, 0,    -4,         0,      0,      0},
        {-2, 1, 0, 0, 0,    -4,         0,      0,      0},
        {1, 0, 0, 0, 0,     -4,         0,      0,      0},
        {0, 0, 1, 2, 0,     3,          0,      0,      0},
        {0, 0, -2, 2, 2,    -3,         0,      0,      0},
        {-1, -1, 1, 0, 0,   -3,         0,      0,      0},
        {0, 1, 1, 0, 0,     -3,         0,      0,      0},
        {0, -1, 1, 2, 2,    -3,         0,      0,      0},
        {2, -1, -1, 2, 2,   -3,         0,      0,      0},
        {0, 0, 3, 2, 2,     -3,         0,      0,      0},
        {2, -1, 0, 2, 2,    -3,         0,      0,      0}
    };

    public final static double AU = 149597870691.0;

    public final static double RAD = Math.PI / 180;

    public static double rad2mard(double rad) {
        rad = rad % (2 * Math.PI);  
        if (rad < 0)  
        return rad + 2 * Math.PI;  
        
        return rad;  
    }

    /**
     * 计算 5 个基本角距
     * @param jd 儒略日数
     * @return
     */
    public static BaseParam calcFiveBaseParam(double jd) {

        double T = ((jd - 2451545) / 365250.0) * 10; /*T是从J2000起算的儒略世纪数*/
        double T2 = T * T;
        double T3 = T2 * T;
        /*平距角（如月对地心的角距离）*/
        double D = 297.85036 + 445267.111480 * T - 0.0019142 * T2 + T3 / 189474.0;
        /*太阳（地球）平近点角*/
        double M = 357.52772 + 35999.050340 * T - 0.0001603 * T2 - T3 / 300000.0;
        /*月亮平近点角*/
        double MD = 134.96298 + 477198.867398 * T + 0.0086972 * T2 + T3 / 56250.0;
        /*月亮纬度参数*/
        double F = 93.27191 + 483202.017538 * T - 0.0036825 * T2 + T3 / 327270.0;
        /*黄道与月亮平轨道升交点黄经*/
        double O = 125.04452 - 1934.136261 * T + 0.0020708 * T2 + T3 / 450000.0;

        BaseParam p = new BaseParam();
        p.D = D;
        p.M = M;
        p.MD = MD;
        p.F = F;
        p.O = O;
        return p;
    }


    /**
     * 章动经度,纬度
     * @param jd 儒略日数
     * @return
     */
    public static double[] calcNuations(double jd){
        BaseParam p = calcFiveBaseParam(jd);
        double D = p.D;
        double M = p.M;
        double MD = p.MD;
        double F = p.F;
        double O = p.O;
        // 章动，单位为 0.0001"
        double lon = 0.0;
        double obl = 0.0;

        double T = ((jd - 2451545) / 365250) * 10;
        for(int i = 0; i < nuationTable.length; i++) {
            // sita  单位为 度
            double sita = nuationTable[i][0] * D + nuationTable[i][1] * M + nuationTable[i][2] * MD + nuationTable[i][3] * F + nuationTable[i][4] * O;
            // 转换为弧度
            sita = sita * RAD;
            lon += (nuationTable[i][5] + nuationTable[i][6] * T) * Math.sin(sita);
            obl += (nuationTable[i][7] + nuationTable[i][8] * T) * Math.sin(sita);
        }
        // 最终转换为度
        return new double[] {lon * 0.0001/3600.0, obl * 0.0001/3600};
    }

    public static double gxc(double R) {
        return 20.49552 / (R * 3600);
    }

    /**
     * 得到地心黄经，某天的准确角度
     * @param coor
     * @param t
     * @return 角度，单位度
     */
    public static double calSunExlipticLongitudeEC(double jd) {

        Coordinate coor = VSOP87.calCoordinate(jd);

        // 太阳地心黄经
        double sunLongitude = (coor.getL() + Math.PI) / RAD;

        //  修正光行查
        double gxc = gxc(coor.getR());
        sunLongitude -= gxc;

        // 修正天体章动
        double[] nua = calcNuations(jd);
        sunLongitude += nua[0];
        sunLongitude = sunLongitude % 360;
        if(sunLongitude < 0) {
            sunLongitude += 360;
        }

        return sunLongitude;

    }

    /**
     * 返回立春大概时间范围
     * @param year
     * @param angle
     * @return
     */
    public static double getTimeScope(int year, int angle) {
        double jd = 365.2425 * (year - 2000) + JDUtils.JD2000;
        int i = angle / 15;
        return jd + i * 15.2 + 120;
    }

    /**
     * 计算黄经角度对应的节气
     * @param year
     * @param angle
     * @return
     */
    public static double calcSolarTerms(int year, int angle){
        double JD0, JD1,stDegree,stDegreep;
        JD1 = getTimeScope(year, angle);
        do {
            JD0 = JD1;
            System.out.println(JDUtils.fromJujian(JD0));
            stDegree = calSunExlipticLongitudeEC(JD0);
            stDegree -= angle;
            // 函数在 JD0 的斜率
            stDegreep = (calSunExlipticLongitudeEC(JD0 + 0.000005) - calSunExlipticLongitudeEC(JD0 - 0.000005)) / 0.00001;

            JD1 = JD0 - stDegree / stDegreep;
        }while((Math.abs(JD1 - JD0) > 0.0000001));

        return JD1;
    }

    static class BaseParam{
        // 平距角
        public double D;
        // 太阳（地球）平近点角
        public double M;
        // 月球平近点角
        public double MD;
        // 月球纬度参数
        public double F;
        // 黄道与月球平轨道升交点黄经
        public double O;
    }


}
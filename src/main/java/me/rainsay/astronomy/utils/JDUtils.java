package me.rainsay.astronomy.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class JDUtils {
    public static final double JD2000 = 2451545;

    /*世界时与原子时之差计算表*/  
    private static final double[] dts = {  
        -4000, 108371.7, -13036.80, 392.000, 0.0000,
        -500, 17201.0,  -627.82, 16.170, -0.3413,
        -150, 12200.6, -346.41, 5.403, -0.1593,  
        150, 9113.8, -328.13, -1.647, 0.0377,
        500, 5707.5, -391.41, 0.915, 0.3145,
        900, 2203.4, -283.45, 13.034, -0.1778,
        1300, 490.1, -57.35, 2.085, -0.0072,
        1600, 120.0, -9.81, -1.532, 0.1403,
        1700, 10.2, -0.91, 0.510, -0.0370,
        1800, 13.4, -0.72, 0.202, -0.0193,
        1830, 7.8, -1.81, 0.416, -0.0247, 
        1860, 8.3, -0.13, -0.406, 0.0292,
        1880, -5.4, 0.32, -0.183, 0.0173,
        1900, -2.3, 2.06, 0.169, -0.0135,
        1920, 21.2, 1.69, -0.304, 0.0167, 
        1940, 24.2, 1.22, -0.064, 0.0031,
        1960, 33.2, 0.51, 0.231, -0.0109,
        1980, 51.0, 1.29, -0.026, 0.0032,
        2000, 64.7, -1.66, 5.224, -0.2905,
        2150, 279.4, 732.95, 429.579, 0.0158,  
        6000
    };

    /**
     * 计算世界时与原子时之差，
     * @param jd 儒略日
     * @return 时间差，单位：日
     */

    public static double deltatT(double jd) {
        jd = jd - JD2000;
        double y = jd / 365.2425 + 2000;
        int i;
        for (i = 0; i < 100; i += 5) {
            if (y < dts[i + 5] || i == 95)  
                break;  
        }
        
        double t1 = (y - dts[i]) / (dts[i + 5] - dts[i]) * 10;  
        double t2 = t1 * t1;  
        double t3 = t2 * t1;  
        double T = dts[i + 1] + dts[i + 2] * t1 + dts[i + 3] * t2 + dts[i + 4] * t3;  
        return T/86400.0;
    }
    /**
     * 计算从公元前4713年1月1日12：00开始的儒略日天数
     */

    public static double julianCal(int year, int month, int day, int hour, int min, int sec) {
        int B = -2;
        if(month <= 2) {
            month += 12;
            year --;
        }
        // 如果是格里历
        if(year > 1582 || year==1582 && (month > 10 || month == 10 && day>=15)) {
            B = year/400 - year/100;
        }
        return (Math.floor(365.25 * year) + Math.floor(30.6001 * (month + 1)) + B + 1720996.5 + day + hour/24.0 + min/1440.0 + sec/86400.0);
    }

    public static double julianCal(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int zone = cal.get(Calendar.ZONE_OFFSET);
        cal.add(Calendar.MILLISECOND, -zone);

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);
        return julianCal(year, month, day, hour, min, sec);
    }

    public static Date utcFromJD(double jd) {
        jd -= deltatT(jd);
        return fromJujian(jd);
    }

    public static Date fromJujian(double jd) {
        double JD = jd + 0.5;
        double Z = Math.floor(JD);
        double F = JD - Z;
        double A;
        if(Z < 2299161) {
            A = Z;
        } else {
            double a = Math.floor((Z - 2305447.5) / 36524.25);
            A = Z + 10 + a -Math.floor(a / 4);
        }
        double B = A + 1524;
        int C = (int)((B - 122.1) / 365.25);
        int D = (int)(365.25 * C);
        int E = (int)((B -D) / 30.600001);
        double jday = B - D - (int)(30.6 * E) + F;
        int day = (int) jday;
        double sec = (jday - day) * 86400.0;
        
        int hour = (int)(sec/3600.0);
        int min = (int)((sec - hour * 3600) / 60.0);
        int second = (int)(sec - hour * 3600 - min * 60);
        int month = 0;
        if(E < 14) {
            month = E - 1;
        } else if(E < 16) {
            month = E - 13;
        }
        
        int year = 0;
        if(month > 2) {
            year = C - 4716;
        }else if(month == 1 || month ==2) {
            year = C - 4715;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        cal.set(year, month - 1, day, hour, min, second);
        int timeOffset = cal.get(Calendar.ZONE_OFFSET);
        cal.add(Calendar.MILLISECOND, timeOffset);
        return cal.getTime();
    }


}
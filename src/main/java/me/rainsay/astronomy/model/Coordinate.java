package me.rainsay.astronomy.model;

import java.text.NumberFormat;

public class Coordinate {

    private Double l;

    private Double b;

    private Double r;

    public Double getL() {
        return l;
    }

    public void setL(Double l) {
        this.l = l;
    }

    public Double getB() {
        return b;
    }

    public void setB(Double b) {
        this.b = b;
    }

    public Double getR() {
        return r;
    }

    public void setR(Double r) {
        this.r = r;
    }

    @Override
    public String toString() {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        nf.setMinimumFractionDigits(10);
        return "Coordinate [L=" + nf.format(l) + ", B=" + nf.format(b) + ", R=" + nf.format(r) + "]";
    }

    public Coordinate(Double l, Double b, Double r) {
        this.l = l;
        this.b = b;
        this.r = r;
    }

    
    
}